<?php
require_once("config.php");

session_start();
if (empty($_SESSION['access_token'])){
	header('Location: https://accounts.google.com/o/oauth2/auth?redirect_uri='.REDIRECT_URI.'&response_type=code&client_id='.CLIENT_ID.'&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fadsense+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fadsense.readonly&approval_prompt=force&access_type=offline');
	exit;
}

?>
<!doctype html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<title>CashFlow</title>
	<link rel="stylesheet" href="css/foundation.css">
	<script src="js/vendor/custom.modernizr.js"></script>
</head>
<body>
	<input type="hidden" id="code" value="<?php echo $code; ?>">
	<div class="row">
		<div class="large-12 columns">
			<div class="large-6 columns">
				<h2>Bienvenue sur CashFlow</h2>
			</div>
			<div class="large-1 columns" id="preloader" style="padding-top: 20px;">
				<img src="img/loader.gif" width="30" >
			</div>
			<div class="large-5 columns alert-box alert" id="error" style="margin-top: 20px; margin-bottom: 30px;">
				Message d'erreur
			</div>
		</div>
	</div>
	<div class="row collapse">
		<div class="large-12 columns">
			<form class="custom" action="#">
				<div class="large-1 columns">
					<span class="prefix">Du</span>
				</div>
				<div class="large-2 columns">
					<input name="startDate" id="startDate" type="date" class="refresh">
				</div>
				<div class="large-1 columns">
					<span class="prefix">Au</span>
				</div>
				<div class="large-2 columns">
					<input name="endDate" id="endDate" type="date" class="refresh">
				</div>
				<div class="large-2 columns">
					<select id="site" class="refresh small">
						<option DISABLED>Choisissez le site</option>
						<option title="Display" value="AD_REQUESTS">ordissimo.fr</option>
					</select>
				</div>
				<div class="large-2 columns">
					<select id="metric" class="refreshgraph small">
						<option DISABLED>Choisissez une métrique</option>
						<option title="€" value="EARNINGS">€</option>
						<option title="Display" value="AD_REQUESTS">Display</option>
						<option title="CTR" value="AD_REQUESTS_CTR">CTR</option>
						<option title="CPC" value="COST_PER_CLICK">CPC</option>
						<option title="RPM" value="AD_REQUESTS_RPM">RPM</option>
					</select>
				</div>
			</div>
		</form>
		<hr />
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<div class="row">
			<div class="large-12 columns">
				<div class="panel" id="graph">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<div class="panel" id="content">
<table id="table">
  <thead>
    <tr>
      <th width="200">Date</th>
      <th>Impressions</th>
      <th width="150">Clics</th>
      <th width="150">CTR</th>
      <th width="150">CPC</th>
      <th width="150">RPM</th>
      <th width="150">Revenus</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
				</div>
			</div>
		</div>
	</div>
</div>


<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.min.js"></script>
<script src="js/foundation/foundation.dropdown.js"></script>
<script src="js/foundation/foundation.forms.js"></script>
  <!--
  
  <script src="js/foundation/foundation.js"></script>
  
  <script src="js/foundation/foundation.alerts.js"></script>
  
  <script src="js/foundation/foundation.clearing.js"></script>
  
  <script src="js/foundation/foundation.cookie.js"></script>
  
  <script src="js/foundation/foundation.joyride.js"></script>
  
  <script src="js/foundation/foundation.magellan.js"></script>
  
  <script src="js/foundation/foundation.orbit.js"></script>
  
  <script src="js/foundation/foundation.reveal.js"></script>
  
  <script src="js/foundation/foundation.section.js"></script>
  
  <script src="js/foundation/foundation.tooltips.js"></script>
  
  <script src="js/foundation/foundation.topbar.js"></script>
  
  <script src="js/foundation/foundation.interchange.js"></script>
  
  <script src="js/foundation/foundation.placeholder.js"></script>
  
  <script src="js/foundation/foundation.abide.js"></script>
  
-->

<script>
$(document).foundation();
$('#preloader').hide();
$('#error').hide();
$("#startDate").val("<?php echo strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d')-7, date('y'))); ?>");
$("#endDate").val("<?php echo strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d')-1, date('y'))); ?>");
</script>
<script src="js/highstock.js"></script>
<script src="js/exporting.js"></script>
<script src="js/ajax.js"></script>
</body>
</html>
