$(function(){
    $('#preloader').show();
    $.get('data.php', 
        {startDate: $("#startDate").val(), endDate: $("#endDate").val(), metric: $("#metric").val()},
        mainController);

    $(".refresh").change( function() {
        $('#preloader').show();
        $.get('data.php', 
            {startDate: $("#startDate").val(), endDate: $("#endDate").val(), metric: $("#metric").val()},
            mainController);
        return false;
    });

    $(".refreshgraph").change( function() {
        $('#preloader').show();
        lang();
        if($("#metric").val() == "EARNINGS"){
            graphEARNINGS(Adsense, Criteo);
        }
        if($("#metric").val() == "AD_REQUESTS"){
            graphAD_REQUESTS(Adsense, Criteo);
        }
        if($("#metric").val() == "AD_REQUESTS_CTR"){
            graphAD_REQUESTS_CTR(Adsense, Criteo);
        }
        if($("#metric").val() == "COST_PER_CLICK"){
            graphCOST_PER_CLICK(Adsense, Criteo);
        }    
        if($("#metric").val() == "AD_REQUESTS_RPM"){
            graphAD_REQUESTS_RPM(Adsense, Criteo);
        }
        $('#preloader').hide();
    });

    return false;
});

var Adsense = new Object();
Adsense.date = new Array();
Adsense.displays = new Array();
Adsense.CTR = new Array();
Adsense.CPC = new Array();
Adsense.RPM = new Array();
Adsense.revenue = new Array();

var Criteo = new Object();
Criteo.displays = new Array();
Criteo.CTR = new Array();
Criteo.CPC = new Array();
Criteo.RPM = new Array();
Criteo.revenue = new Array();

var Total = new Object();
Total.displays = new Array();
Total.CTR = new Array();
Total.CPC = new Array();
Total.RPM = new Array();
Total.revenue = new Array();

var TimeRangeTotal = new Object();

function mainController(data){
    console.log(data);
    if (data.status != "success")
    {
        $('#preloader').hide();
        $("#error").html(data.source+" : "+data.message).show();
        return;
    }

    $("#error").hide();

    table(data);
    lang();
    if($("#metric").val() == "EARNINGS"){
        graphEARNINGS(Adsense, Criteo);
    }
    if($("#metric").val() == "AD_REQUESTS"){
        graphAD_REQUESTS(Adsense, Criteo);
    }
    if($("#metric").val() == "AD_REQUESTS_CTR"){
        graphAD_REQUESTS_CTR(Adsense, Criteo);
    }
    if($("#metric").val() == "COST_PER_CLICK"){
        graphCOST_PER_CLICK(Adsense, Criteo);
    }    
    if($("#metric").val() == "AD_REQUESTS_RPM"){
        graphAD_REQUESTS_RPM(Adsense, Criteo);
    }
    $('#preloader').hide();
}

function table(data){
    $('.tr').remove();
    Adsense.date = [];
    Adsense.displays = [];
    Adsense.CTR = [];
    Adsense.CPC = [];
    Adsense.RPM = [];
    Adsense.revenue = [];
    Criteo.displays = [];
    Criteo.CTR = [];
    Criteo.CPC = [];
    Criteo.RPM = [];
    Criteo.revenue = [];
    Total.displays = [];
    Total.CTR = [];
    Total.CPC = [];
    Total.RPM = [];
    Total.revenue = [];
    Total.clics = [];
    TimeRangeTotal.displays = 0;
    TimeRangeTotal.CTR = 0;
    TimeRangeTotal.CPC = 0;
    TimeRangeTotal.RPM = 0;
    TimeRangeTotal.revenue = 0;
    TimeRangeTotal.clics = 0;


    for (var i = 0; i < data.adsense.rows.length; i++) 
    {
        /** CRITEO **/
        Criteo.revenue[i] = 0;
        Criteo.displays[i] = 0;
        Criteo.CTR[i] = 0;
        Criteo.CPC[i] = 0;
        Criteo.RPM[i] = 0;
    }

    for (var i = 0; i < data.adsense.rows.length; i++) 
    {
        /** ADSENSE **/
        Adsense.date[i] = data.adsense.rows[i][0];
        Adsense.displays[i] = parseInt(data.adsense.rows[i][1]*data.adsense.rows[i][6]);
        Adsense.CTR[i] = parseFloat(parseFloat(data.adsense.rows[i][2]*100).toFixed(2));
        Adsense.CPC[i] = parseFloat(data.adsense.rows[i][3]);
        Adsense.RPM[i] = parseFloat(data.adsense.rows[i][4]);
        Adsense.revenue[i] = parseFloat(data.adsense.rows[i][5]);

        var sum = 0;

        for (var j = 0; j < data.criteo.Data.Zone.length; j++) 
        {
            if(data.criteo.Data.Zone[j].Stats)
            {
                if(data.criteo.Data.Zone[j].Stats[i]) 
                {
                    Criteo.revenue[i+(data.adsense.rows.length - data.criteo.Data.Zone[0].Stats.length)] += parseFloat(parseFloat(data.criteo.Data.Zone[j].Stats[i]["@attributes"].Revenue).toFixed(2));
                    Criteo.displays[i+(data.adsense.rows.length - data.criteo.Data.Zone[0].Stats.length)] += parseFloat(data.criteo.Data.Zone[j].Stats[i]["@attributes"].CriteoDisplay);
                    var CTR = 0;
                    var imps = 0;

                    if(data.criteo.Data.Zone[j].Stats[i]["@attributes"].CTR != "-")
                    {
                        CTR = parseFloat(data.criteo.Data.Zone[j].Stats[i]["@attributes"].CTR.replace('%',''));
                    }
                    else CTR = 0.00;
                    imps = parseFloat(data.criteo.Data.Zone[j].Stats[i]["@attributes"].CriteoDisplay);
                    sum += CTR * imps;
                }else console.log('Données manquante ! Zone: '+j+' Stats: '+i);
            }
        }
        if(data.criteo.Data.Zone[0].Stats[i]) {
            Criteo.CTR[i+(data.adsense.rows.length - data.criteo.Data.Zone[0].Stats.length)] = parseFloat(parseFloat((sum)/Criteo.displays[i+(data.adsense.rows.length - data.criteo.Data.Zone[0].Stats.length)]).toFixed(2));
            Criteo.CPC[i+(data.adsense.rows.length - data.criteo.Data.Zone[0].Stats.length)] = parseFloat(parseFloat((Criteo.revenue[i+(data.adsense.rows.length - data.criteo.Data.Zone[0].Stats.length)] * 100) / (Criteo.CTR[i+(data.adsense.rows.length - data.criteo.Data.Zone[0].Stats.length)] * Criteo.displays[i+(data.adsense.rows.length - data.criteo.Data.Zone[0].Stats.length)])).toFixed(2));
            Criteo.RPM[i+(data.adsense.rows.length - data.criteo.Data.Zone[0].Stats.length)] = parseFloat(parseFloat((Criteo.revenue[i+(data.adsense.rows.length - data.criteo.Data.Zone[0].Stats.length)] * 1000) / Criteo.displays[i+(data.adsense.rows.length - data.criteo.Data.Zone[0].Stats.length)]).toFixed(2));
        }

        if(Criteo.CTR[i] != 0)
        {
            Total.displays[i] = Adsense.displays[i] + Criteo.displays[i];
            Total.revenue[i] = parseFloat(parseFloat(Adsense.revenue[i] + Criteo.revenue[i]).toFixed(2));
            Total.CTR[i] = parseFloat(parseFloat(((Adsense.CTR[i]*Adsense.displays[i])+(Criteo.CTR[i]*Criteo.displays[i]))/(Adsense.displays[i]+Criteo.displays[i])).toFixed(2));
            Total.CPC[i] = parseFloat(parseFloat(((Adsense.revenue[i] + Criteo.revenue[i]) * 100) / (Criteo.CTR[i] * Criteo.displays[i] + Adsense.CTR[i] * Adsense.displays[i])).toFixed(2));
            Total.RPM[i] = parseFloat(parseFloat(((Adsense.revenue[i] + Criteo.revenue[i]) * 1000) / (Adsense.displays[i] + Criteo.displays[i])).toFixed(2));
        }
        else
        {
            Total.displays[i] = Adsense.displays[i];
            Total.revenue[i] = Adsense.revenue[i];
            Total.CTR[i] = Adsense.CTR[i];
            Total.CPC[i] = Adsense.CPC[i];
            Total.RPM[i] = Adsense.RPM[i];
        }

        Total.clics[i] = parseInt(Total.revenue[i] / Total.CPC[i]);

        TimeRangeTotal.displays += Total.displays[i];
        TimeRangeTotal.revenue += Total.revenue[i];
        TimeRangeTotal.CTR += Total.CTR[i];
        TimeRangeTotal.CPC += Total.CPC[i];
        TimeRangeTotal.RPM += Total.RPM[i];
        TimeRangeTotal.clics += Total.clics[i];

        Total.displays[i] = Total.displays[i].toString().replace('.',',');
        Total.revenue[i] = Total.revenue[i].toString().replace('.',',');
        Total.CTR[i] = Total.CTR[i].toString().replace('.',',');
        Total.CPC[i] = Total.CPC[i].toString().replace('.',',');
        Total.RPM[i] = Total.RPM[i].toString().replace('.',',');
    }

    console.log('Adsense');
    console.log(Adsense);
    console.log('Criteo');
    console.log(Criteo);
    console.log('Total');
    console.log(Total);

    for (var i = 0; i < Adsense.date.length; i++) 
    {
        $("#table").append("<tr class=\"tr\"><td>"+Adsense.date[i]+"</td><td>"+Total.displays[i]+"</td><td>"+Total.clics[i]+"</td><td>"+Total.CTR[i]+" %</td><td>"+Total.CPC[i]+" €</td><td>"+Total.RPM[i]+" €</td><td>"+Total.revenue[i]+" €</td></tr>");
    }

    /* average */
    av_displays = parseInt(TimeRangeTotal.displays / Adsense.date.length);
    av_clics = parseInt(TimeRangeTotal.clics / Adsense.date.length);
    av_CTR = parseFloat((TimeRangeTotal.clics * 100) / TimeRangeTotal.displays).toFixed(2).toString().replace('.',',');
    av_CPC = parseFloat(TimeRangeTotal.revenue / TimeRangeTotal.clics).toFixed(2).toString().replace('.',',');
    av_RPM = parseFloat((TimeRangeTotal.revenue * 1000) / TimeRangeTotal.displays).toFixed(2).toString().replace('.',',');
    av_revenue = parseFloat(TimeRangeTotal.revenue / Adsense.date.length).toFixed(2).toString().replace('.',',');
    $("#table").append("<tr class=\"tr\"><td><b>Moyennes</b></td><td>"+av_displays+"</td><td>"+av_clics+"</td><td>"+av_CTR+" %</td><td>"+av_CPC+"€</td><td>"+av_RPM+" €</td><td>"+av_revenue+" €</td></tr>");

    /* totals */
    TimeRangeTotal.displays = TimeRangeTotal.displays.toString().replace('.',',');
    TimeRangeTotal.revenue = TimeRangeTotal.revenue.toFixed(2).toString().replace('.',',');
    $("#table").append("<tr class=\"tr\"><td><b>Totaux</b></td><td>"+TimeRangeTotal.displays+"</td><td>"+TimeRangeTotal.clics+"</td><td></td><td></td><td></td><td>"+TimeRangeTotal.revenue+" €</td></tr>");
}

function lang(){
    Highcharts.setOptions({
        lang: {
            months: ['janvier', 'février', 'mars', 'avril', 'mai', 'juin',
            'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'],
            weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 
            'Jeudi', 'Vendredi', 'Samedi'],
            shortMonths: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil',
            'Août', 'Sept', 'Oct', 'Nov', 'Déc'],
            numericSymbols: [' K', ' M', ' G'],
            downloadPNG: 'Télécharger en image PNG',
            downloadJPEG: 'Télécharger en image JPEG',
            downloadPDF: 'Télécharger en document PDF',
            downloadSVG: 'Télécharger en document Vectoriel',
            exportButtonTitle: 'Export du graphique',
            loading: 'Chargement en cours...',
            printChart: 'Imprimer le graphique',
            resetZoom: 'Réinitialiser le zoom',
            resetZoomTitle: 'Réinitialiser le zoom au niveau 1:1',
            contextButtonTitle: 'Exporter',
            thousandsSep: ' ',
            decimalPoint: ','
        }
    });
}

/******************************************************************************************
 ******************************************************************************************
 *                              EARNINGS
 ******************************************************************************************
 *****************************************************************************************/

 function graphEARNINGS(Adsense, Criteo){

    $('#graph').highcharts({
        chart: {
            type: 'area'
        },

        title: {
            text : 'Revenus'
        },

        plotOptions: {
            area: {
                stacking: 'normal',
                lineColor: '#666666',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#666666'
                }
            }
        },

        colors: [
        '#AA4643', 
        '#4572A7', 
        '#89A54E', 
        '#80699B', 
        ],

        tooltip: {
            shared: true,
            useHTML: true,
            valueDecimals: 2,
            formatter: function() {
                var s = '<small>'+Highcharts.dateFormat('%d/%m/%y',this.x)+'</small><table>',
                sum = 0;
                $.each(this.points, function(i, point) {
                    s += '<tr><td style="color:'+point.series.color+'">'+point.series.name+': </td>' +
            '<td style="text-align: right"><b>'+point.y+' €</b></td></tr>';
                    total = point.total;
                });
                s += '<tr><td style="color: black"><b>Total: </b></td>' +
            '<td style="text-align: right"><b>'+total+' €</b></td></tr>';
                return s;
            },
        },

        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: {
                day: '%e %b'
            },
            title: {
                enabled: false
            },
        },
        
        yAxis: {
            title: {
                text: 'Revenus totaux'
            },
            min : 0
        },

        series: [{
            name: 'Criteo',
            data: Criteo.revenue,
            pointStart: Date.parse($("#startDate").val()),
            pointInterval: 24 * 3600 * 1000

        },{
            name: 'Adsense',
            data: Adsense.revenue,
            pointStart: Date.parse($("#startDate").val()),
            pointInterval: 24 * 3600 * 1000
        }],  

        credits: {
            enabled: true,
            href: 'http://fr.linkedin.com/pub/sofiane-baltaci/39/b07/53',
            text: 'by Sofiane Baltaci'
        }
    });
}

/******************************************************************************************
 ******************************************************************************************
 *                              AD_REQUESTS
 ******************************************************************************************
 *****************************************************************************************/

 function graphAD_REQUESTS(Adsense, Criteo){

    $('#graph').highcharts({
        chart: {
            type: 'area'
        },

        title: {
            text : 'Impressions'
        },

        plotOptions: {
            area: {
                stacking: 'normal',
                lineColor: '#666666',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#666666'
                }
            }
        },

        colors: [
        '#AA4643', 
        '#4572A7', 
        '#89A54E', 
        '#80699B', 
        ],

        tooltip: {
            shared: true,
            useHTML: true,
            valueDecimals: 2,
            formatter: function() {
                var s = '<small>'+Highcharts.dateFormat('%d/%m/%y',this.x)+'</small><table>',
                sum = 0;
                $.each(this.points, function(i, point) {
                    s += '<tr><td style="color:'+point.series.color+'">'+point.series.name+': </td>' +
            '<td style="text-align: right"><b>'+point.y+' vues</b></td></tr>';
                    total = point.total;
                });
                s += '<tr><td style="color: black"><b>Total: </b></td>' +
            '<td style="text-align: right"><b>'+total+' vues</b></td></tr>';
                return s;
            },
        },

        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: {
                day: '%e %b'
            },
            title: {
                enabled: false
            },
        },
        
        yAxis: {
            title: {
                text: 'Nombre d\'impressions'
            },
            min : 0
        },

        series: [{
            name: 'Criteo',
            data: Criteo.displays,
            pointStart: Date.parse($("#startDate").val()),
            pointInterval: 24 * 3600 * 1000

        },{
            name: 'Adsense',
            data: Adsense.displays,
            pointStart: Date.parse($("#startDate").val()),
            pointInterval: 24 * 3600 * 1000
        }],  

        credits: {
            enabled: true,
            href: 'http://fr.linkedin.com/pub/sofiane-baltaci/39/b07/53',
            text: 'by Sofiane Baltaci'
        }
    });
}

/******************************************************************************************
 ******************************************************************************************
 *                              AD_REQUESTS_CTR
 ******************************************************************************************
 *****************************************************************************************/

 function graphAD_REQUESTS_CTR(Adsense, Criteo){

    $('#graph').highcharts({
        chart: {
            type: 'area'
        },

        title: {
            text : 'CTR'
        },

        plotOptions: {
            area: {
                stacking: null,
                lineColor: '#666666',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#666666'
                }
            }
        },

        colors: [
        '#AA4643', 
        '#4572A7', 
        '#89A54E', 
        '#80699B', 
        ],

tooltip: {
            shared: true,
            useHTML: true,
            valueDecimals: 2,
            formatter: function() {
                var s = '<small>'+Highcharts.dateFormat('%d/%m/%y',this.x)+'</small><table>',
                sum = 0;
                $.each(this.points, function(i, point) {
                    s += '<tr><td style="color:'+point.series.color+'">'+point.series.name+': </td>' +
            '<td style="text-align: right"><b>'+point.y+' %</b></td></tr>';
                    total = point.total;
                });
                s += '<tr><td style="color: black"><b>Total: </b></td>' +
            '<td style="text-align: right"><b>'+total+' %</b></td></tr>';
                return s;
            },
        },

        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: {
                day: '%e %b'
            },
            title: {
                enabled: false
            },
        },
        
        yAxis: {
            title: {
                text: 'Taux de clics par affichages'
            },
            min : 0
        },

        series: [{
            name: 'Criteo',
            data: Criteo.CTR,
            pointStart: Date.parse($("#startDate").val()),
            pointInterval: 24 * 3600 * 1000

        },{
            name: 'Adsense',
            data: Adsense.CTR,
            pointStart: Date.parse($("#startDate").val()),
            pointInterval: 24 * 3600 * 1000
        }],  

        credits: {
            enabled: true,
            href: 'http://fr.linkedin.com/pub/sofiane-baltaci/39/b07/53',
            text: 'by Sofiane Baltaci'
        }
    });
}

/******************************************************************************************
 ******************************************************************************************
 *                              COST_PER_CLICK
 ******************************************************************************************
 *****************************************************************************************/

 function graphCOST_PER_CLICK(Adsense, Criteo){

    $('#graph').highcharts({
        chart: {
            type: 'area'
        },

        title: {
            text : 'CPC'
        },

        plotOptions: {
            area: {
                stacking: null,
                lineColor: '#666666',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#666666'
                }
            }
        },

        colors: [
        '#AA4643', 
        '#4572A7', 
        '#89A54E', 
        '#80699B', 
        ],

tooltip: {
            shared: true,
            useHTML: true,
            valueDecimals: 2,
            formatter: function() {
                var s = '<small>'+Highcharts.dateFormat('%d/%m/%y',this.x)+'</small><table>',
                sum = 0;
                $.each(this.points, function(i, point) {
                    s += '<tr><td style="color:'+point.series.color+'">'+point.series.name+': </td>' +
            '<td style="text-align: right"><b>'+point.y+' €/clic</b></td></tr>';
                    total = point.total;
                });
                s += '<tr><td style="color: black"><b>Total: </b></td>' +
            '<td style="text-align: right"><b>'+total+' €/clic</b></td></tr>';
                return s;
            },
        },

        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: {
                day: '%e %b'
            },
            title: {
                enabled: false
            },
        },
        
        yAxis: {
            title: {
                text: 'Coût par clic'
            },
            min : 0
        },

        series: [{
            name: 'Criteo',
            data: Criteo.CPC,
            pointStart: Date.parse($("#startDate").val()),
            pointInterval: 24 * 3600 * 1000

        },{
            name: 'Adsense',
            data: Adsense.CPC,
            pointStart: Date.parse($("#startDate").val()),
            pointInterval: 24 * 3600 * 1000
        }],  

        credits: {
            enabled: true,
            href: 'http://fr.linkedin.com/pub/sofiane-baltaci/39/b07/53',
            text: 'by Sofiane Baltaci'
        }
    });
}

/******************************************************************************************
 ******************************************************************************************
 *                              AD_REQUESTS_RPM
 ******************************************************************************************
 *****************************************************************************************/

 function graphAD_REQUESTS_RPM(Adsense, Criteo){

    $('#graph').highcharts({
        chart: {
            type: 'area'
        },

        title: {
            text : 'RPM'
        },

        plotOptions: {
            area: {
                stacking: null,
                lineColor: '#666666',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#666666'
                }
            }
        },

        colors: [
        '#AA4643', 
        '#4572A7', 
        '#89A54E', 
        '#80699B', 
        ],

tooltip: {
            shared: true,
            useHTML: true,
            valueDecimals: 2,
            formatter: function() {
                var s = '<small>'+Highcharts.dateFormat('%d/%m/%y',this.x)+'</small><table>',
                sum = 0;
                $.each(this.points, function(i, point) {
                    s += '<tr><td style="color:'+point.series.color+'">'+point.series.name+': </td>' +
            '<td style="text-align: right"><b>'+point.y+' €</b></td></tr>';
                    total = point.total;
                });
                s += '<tr><td style="color: black"><b>Total: </b></td>' +
            '<td style="text-align: right"><b>'+total+' €</b></td></tr>';
                return s;
            },
        },

        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: {
                day: '%e %b'
            },
            title: {
                enabled: false
            },
        },
        
        yAxis: {
            title: {
                text: 'Revenus pour mille affichages'
            },
            min : 0
        },

        series: [{
            name: 'Criteo',
            data: Criteo.RPM,
            pointStart: Date.parse($("#startDate").val()),
            pointInterval: 24 * 3600 * 1000

        },{
            name: 'Adsense',
            data: Adsense.RPM,
            pointStart: Date.parse($("#startDate").val()),
            pointInterval: 24 * 3600 * 1000
        }],  

        credits: {
            enabled: true,
            href: 'http://fr.linkedin.com/pub/sofiane-baltaci/39/b07/53',
            text: 'by Sofiane Baltaci'
        }
    });
}
