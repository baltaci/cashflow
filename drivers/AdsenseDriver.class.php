<?php
require_once("config.php");

/**
* 
*/
class AdsenseDriver
{
	public $response;
	
	function __construct($account_id, $startDate, $endDate)
	{
		$url  = 'https://www.googleapis.com/adsense/v1.3/reports?';
		$url .= '&startDate='.$startDate;
		$url .= '&endDate='.$endDate;
		$url .= '&accountId='$account_id;
		$url .= '&metric=AD_REQUESTS';
		$url .= '&metric=AD_REQUESTS_CTR';
		$url .= '&metric=COST_PER_CLICK';
		$url .= '&metric=AD_REQUESTS_RPM';
		$url .= '&metric=EARNINGS';
		$url .= '&metric=AD_REQUESTS_COVERAGE';
		$url .= '&dimension=DATE';
		$url .= '&filter=PRODUCT_CODE%3D%3DAFC';
		$url .= '&useTimezoneReporting=true';
		$response = http_parse_message(http_get($url, array('headers' => array('Authorization' => 'OAuth '.$_SESSION['access_token'])), $info));
		if (! $response || ($response->responseCode != 200)) {
			if (isset($response->responseStatus))
				$message = "{$response->responseStatus} ({$response->responseCode})";
			else
				$message = 'Impossible to contact the server';
			send_error('adsense', $message);
		}

		$this->response = json_decode($response->body);
	}

	/* Google Token refresh */ 
	public function token_refresh()
	{
		if(isset($this->response->error->code)){
			if($this->response->error->code == 401){
				$url = 'https://accounts.google.com/o/oauth2/token';
				$response = http_parse_message(http_post_data($url, 'client_id='.CLIENT_ID.'&client_secret='.CLIENT_SECRET.'&refresh_token='.$_SESSION['refresh_token'].'&grant_type=refresh_token'));
				$auth = json_decode($response->body);
				$_SESSION['access_token'] = $auth->access_token;
				$_SESSION['token_type'] = $auth->token_type;
				$_SESSION['expires_in'] = $auth->expires_in;
				return true;
			}
			else {
				$error = array('status' => 'error',
					'source' => 'adsense',
					'message' => $adsense->error->message);
				send_error('adsense', $adsense->error->message);
			}
		}
	}
}