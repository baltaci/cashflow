<?php
require_once("config.php");

	/**
	* 
	*/
	class CriteoDriver
	{
		public $response;

		function __construct($apitoken, $site_id, $startDate, $endDate, $zone_id = null)
		{
			$url = 'https://publishers.criteo.com/statsexport.aspx?apitoken='.$apitoken;
			$url .= '&begindate='.$startDate;
			$url .= '&enddate='.$endDate;
			$url .= '&siteid='.$site_id;
			if(isset($zone_id)) 
				$url .= '&zoneid=';
			$url .= '&format=xml';
			$this->response = file_get_contents($url);
			if (!$this->response) send_error('criteo', 'Impossible to get data from the server');
		}

		public function clean()
		{
			$xml = str_replace(array("\n", "\r", "\t"), '', $xml);
			$xml = trim(str_replace('"', "'", $xml));
			$criteo = simplexml_load_string($xml);
			if (!$criteo) {
				send_error('criteo', 'Impossible to parse XML data');
			}
		}
	}