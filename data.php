<?php
require_once("config.php");

function send_data($data) {
	$data = json_encode($data);
	header('Content-Type: application/json');
	echo $data;
	exit;
}

function send_error($source, $message) {
	$error = array('status' => 'error',
	               'source' => $source,
	               'message' => $message);
	send_data($error);
}

if (!empty($_GET['code'])){

	$url = 'https://accounts.google.com/o/oauth2/token';
	$response = http_parse_message(http_post_data($url, 'code='.$_GET['code'].'&redirect_uri='.REDIRECT_URI.'&client_id='.CLIENT_ID.'&scope=&client_secret='.CLIENT_SECRET.'&grant_type=authorization_code'));
	$auth = json_decode($response->body);

	session_start();
	$_SESSION['access_token'] = $auth->access_token;
	$_SESSION['token_type'] = $auth->token_type;
	$_SESSION['expires_in'] = $auth->expires_in;
	$_SESSION['refresh_token'] = $auth->refresh_token;

	header('Location: index.php');
	exit;

}else if (!empty($_GET['startDate']) && !empty($_GET['endDate'])){

	session_start();

	$Adsense = new AdsenseDriver(ACCOUNT_ID, $_GET['startDate'], $_GET['endDate']);
	$Adsense->token_refresh();
	$data['adsense'] = $Adsense->response;
	
	$Criteo = new CriteoDriver(APITOKEN, SITE_ID, $_GET['startDate'], $_GET['endDate']);
	$Criteo->clean();
	$data['criteo'] = $Criteo->response;

	$data['status'] = 'success';
	$data['info'] = $_SESSION;
	$data['request'] = $_GET;

	send_data($data);

}else{
	header('Location: https://accounts.google.com/o/oauth2/auth?redirect_uri='.REDIRECT_URI.'&response_type=code&client_id='.CLIENT_ID.'&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fadsense+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fadsense.readonly&approval_prompt=force&access_type=offline');
	exit;
}
?>
